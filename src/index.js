import Tabulator from "tabulator-tables";

import "../node_modules/tabulator-tables/dist/css/bootstrap/tabulator_bootstrap4.min.css";

const tabledata = require("./gmdb.json");

const theColumns = require("./columns.json");

function sortIds(
  id1,
  id2,
  aRow, // eslint-disable-line no-unused-vars
  bRow, // eslint-disable-line no-unused-vars
  column, // eslint-disable-line no-unused-vars
  dir, // eslint-disable-line no-unused-vars
  sorterParams // eslint-disable-line no-unused-vars
) {
  const s1 = id1.replace(/^NETL-MAT-/, "");
  const s2 = id2.replace(/^NETL-MAT-/, "");
  const n1 = parseInt(s1, 10);
  const n2 = parseInt(s2, 10);
  return n1 - n2;
}

theColumns[1].sorter = sortIds;

/*
function selectRow(e, row) {
  const data = row.getData();

  let details = '';

  Object.keys(data).forEach((property) => {
    const currCol = theColumns.find((prop) => prop.field === property);
    // const matDetails = currCol.detailed_title;
    const matDetails = data[property];
    if (currCol) {
      details += `${matDetails}:   ${data[property]}; <br/>`;
    }
  });
  document.getElementById('details').innerHTML = details;
}
*/

// create Tabulator on DOM element with id 'example-table'
const table = new Tabulator("#example-table", {
  height: "80%", // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
  data: tabledata, // assign data to table
  layout: "fitColumns", // fit columns to width of table (optional)
  movableColumns: true,
  columns: theColumns,
  // rowClick: selectRow,
});

// Define variables for input elements
const valueEl = document.getElementById("filter-value");

document.getElementById("download-csv").addEventListener("click", () => {
  table.download("csv", "data.csv");
});

document.getElementById("download-xlsx").addEventListener("click", () => {
  table.download("xlsx", "granular_materials.xlsx", {
    sheetName: "Granular Materials",
  });
});

// Trigger setFilter function with correct parameters
function updateFilter() {
  const filterVal = "Material_Name";
  const typeVal = "like";

  valueEl.disabled = false;

  table.setFilter(filterVal, typeVal, valueEl.value);
}

// Update filters on value change
document.getElementById("filter-value").addEventListener("keyup", updateFilter);

const cols = document.getElementById("columnlist");

for (let ii = 0; ii < theColumns.length; ii += 1) {
  const column = theColumns[ii];
  const checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.id = `col_${column.field}`;
  checkbox.name = `col_${column.field}`;
  checkbox.value = `col_${column.field}`;
  checkbox.checked = column.visible;
  const checklabel = document.createElement("label");
  checklabel.for = `col_${column.field}`;
  checklabel.innerHTML = `${column.title}`;

  checkbox.addEventListener("click", () => {
    table.getColumns()[ii].toggle();
  });
  cols.appendChild(checkbox);
  cols.appendChild(checklabel);
}
