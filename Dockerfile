FROM node:15.6.0-buster-slim

LABEL maintainer="Mark Meredith <mark.meredith@netl.doe.gov>"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update \
  && apt-get -qq -y install --no-install-recommends \
  git=1:2.20.1-2+deb10u3 \
  awscli=1.16.113-1 \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

CMD [ "/bin/bash" ]
