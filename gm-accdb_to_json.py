import json
from enum import Enum

import pyodbc

ACCDB_FILENAME = r"C:\Users\mmeredith\repos\materials-db\data.accdb;"


class SizeShapeFields(Enum):
    """ Fields of Size and Shape table"""

    rev = "Rev"
    SMD = "SMD"
    SMD_Err = "SMD Error"
    d5 = "d5"
    d95 = "d95"
    Sphr = "Sphericity"
    Sphr_Err = "Sphericty Error"
    s10 = "s10"
    s90 = "s90"
    AR = "AR"
    AR_Err = "AR Error"
    ar10 = "ar10"
    ar90 = "ar90"


class PycnometerFields(Enum):
    """ Fields of Pycnometer Data table"""

    rev = "Rev"
    Rho_Skel = "Skeletal Density"
    Rho_Skel_Err = "Skeletal Density Err"


class FluidbedFields(Enum):
    """ Fields of Fluid Bed Data table"""

    Umf = "Umf"
    Umf_Error = "Umf Error"
    BF_F = "Bulk Density - Fluffed"
    BF_F_Err = "BD Fluffed Error"
    BF_P = "Bulk Density - Packed"
    BF_P_Err = "BD Packed Error"


class CalculatedFields(Enum):
    """ Fields of Calculated Data table"""

    Rho_Part = "Particle Density"
    Rho_Part_Err = "Particle Density Error"
    VF_F = "Void Fraction - Fluffed"
    VF_F_Err = "VF Fluffed Error"
    VF_P = "Void Fraction - Packed"
    VF_P_Err = "VF Packed Error"
    Por = "Porosity"
    Por_Err = "Porosity Error"
    Geldart = "Geldart Classification"


def main():
    cursor = get_cursor()
    mat_idx = cursor.execute('select * from "Material Index"')
    materials = dict()
    for row in mat_idx:
        mat_id, name, *_ = row
        material = {
            "Material_ID": mat_id,
            "Material_Name": name,
        }
        materials[mat_id] = material

    for table in get_tables(cursor):
        print(f"{table} columns are:::   ", get_columns(cursor, table))

    sizeshapes = dict()
    cursor.execute("select * from [Size and Shape]")
    for row in cursor.fetchall():
        mat_id = row.__getattribute__("Material ID")
        if mat_id in materials:
            sizeshapes[mat_id] = {
                member.value: row.__getattribute__(member.value)
                for member in SizeShapeFields
            }

    pycno = dict()
    cursor.execute("select * from [Pycnometer Data]")
    for row in cursor.fetchall():
        mat_id = row.__getattribute__("Material ID")
        if mat_id in materials:
            pycno[mat_id] = {
                member.value: row.__getattribute__(member.value)
                for member in PycnometerFields
            }

    fb_data = dict()
    cursor.execute("select * from [Fluid Bed Data]")
    for row in cursor.fetchall():
        mat_id = row.__getattribute__("Material ID")
        if mat_id in materials:
            fb_data[mat_id] = {
                member.value: row.__getattribute__(member.value)
                for member in FluidbedFields
            }

    calc_data = dict()
    cursor.execute("select * from [Calculated Data]")
    for row in cursor.fetchall():
        mat_id = row.__getattribute__("Material ID")
        if mat_id in materials:
            calc_data[mat_id] = {
                member.value: row.__getattribute__(member.value)
                for member in CalculatedFields
            }

    photomicrographs_data = dict()
    cursor.execute("select * from [Photomicrographs]")
    for row in cursor.fetchall():
        mat_id = row.__getattribute__("Material ID")
        if mat_id in materials:
            photomicrographs_data[mat_id] = {
                "Image": row.__getattribute__("Image"),
            }

    mergedlist = []
    for mat_id in materials:
        mat = materials[mat_id]


        merged = dict()
        merged["Material ID"] = f"NETL-MAT-{mat_id}"
        merged["Material_Name"] = mat["Material_Name"]

        ss = sizeshapes.get(mat_id, {})
        for member in SizeShapeFields:
            merged[member.value] = ss.get(member.value, "")

        pyc = pycno.get(mat_id, {})
        for member in PycnometerFields:
            merged[member.value] = pyc.get(member.value, "")

        fb = fb_data.get(mat_id, {})
        for member in FluidbedFields:
            merged[member.value] = fb.get(member.value, "")

        calc = calc_data.get(mat_id, {})
        for member in CalculatedFields:
            merged[member.value] = calc.get(member.value, "")

        img = photomicrographs_data.get(mat_id, {})
        merged["Image"] = img.get("Image", "")

        mergedlist.append(merged)

    with open("src/gmdb.json", "w", newline="") as jsonfile:
        gm_json = json.dumps(mergedlist, indent=2)
        jsonfile.write(gm_json)


def get_tables(cursor):
    tables = []
    for table in cursor.tables(tableType="TABLE"):
        tablename = table[2]
        tables.append(tablename)
    return tables


def get_columns(cursor, tablename):
    cols = []
    for tname in get_tables(cursor):
        if tablename == tname:
            for col in cursor.columns(table=tablename):
                cols.append(col[3])
            break
    else:
        print("BAD TABLENAME: ", tablename)
    # print("\n\ntable: ", tablename, "has columns: ", end=" ")
    return cols


def get_cursor():
    conn = pyodbc.connect(
        "Driver={Microsoft Access Driver (*.mdb, *.accdb)};DBQ=%s;" % ACCDB_FILENAME
    )
    return conn.cursor()


main()
