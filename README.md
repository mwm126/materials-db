# Granular Materials Database

This repo is for updating the Granular Materials Database on https://mfix.netl.doe.gov.

The database is maintained in a Microsoft Access file. This repo has a Python
script `accdb_to_json.py` for exporting the data from Access to JSON, and then a Wordpress
plugin `materials-updater.php` for updating the Wordpress database with the CSV
data.

## Update JSON from .accdb

The script only runs on Windows, so first you will need a Windows installation of Python.

Edit `accdb_to_json.py` to set `ACCDB_FILENAME` to the path of Access file (e.g. `Granular Materials Database.accdb`)

```shell
C:\> python -m pip install pyodbc
C:\> python accdb_to_json.py
```

This will update `src/gmdb.json`.

## Deploy Update

Commit the changes to `src/gmdb.json` and merge them to branch `main` to update the website.

## Build for local development

Install `npm`.

```shell
> npm install
> npx webpack
```

Then open the `dist/index.html` file in your browser to preview.

Online preview available at: https://mfix.aeolustec.com/granular-material-database/
